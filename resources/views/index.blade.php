<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>ToDo - Vuexy - Bootstrap HTML admin template</title>
    <link rel="apple-touch-icon" href="/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/editors/quill/katex.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/editors/quill/monokai-sublime.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/editors/quill/quill.snow.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/dragula.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/css/extensions/toastr.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/form-quill-editor.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/plugins/forms/form-validation.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/app-todo.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- END: Custom CSS-->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.3.2/axios.min.js" integrity="sha512-NCiXRSV460cHD9ClGDrTbTaw0muWUBf/zB/yLzJavRsPNUl9ODkUVmUHsZtKu17XknhsGlmyVoJxLg/ZQQEeGA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern content-left-sidebar navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-dark navbar-shadow container-xxl">
        <div class="navbar-container d-flex content">
            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav bookmark-icons">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link" href="/" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Todo"><i class="ficon" data-feather="check-square"></i></a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav align-items-center ms-auto">
                <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="sun"></i></a></li>
                <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none"><span class="user-name fw-bolder">Hoàng Công Trường</span><span class="user-status">Admin</span></div><span class="avatar"><img class="round" src="/app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user"><a class="dropdown-item" href="page-profile.html"><i class="me-50" data-feather="user"></i> Profile</a><a class="dropdown-item" href="app-email.html"><i class="me-50" data-feather="mail"></i> Inbox</a><a class="dropdown-item" href="app-todo.html"><i class="me-50" data-feather="check-square"></i> Task</a><a class="dropdown-item" href="app-chat.html"><i class="me-50" data-feather="message-square"></i> Chats</a>
                        <div class="dropdown-divider"></div><a class="dropdown-item" href="page-account-settings-account.html"><i class="me-50" data-feather="settings"></i> Settings</a><a class="dropdown-item" href="page-pricing.html"><i class="me-50" data-feather="credit-card"></i> Pricing</a><a class="dropdown-item" href="page-faq.html"><i class="me-50" data-feather="help-circle"></i> FAQ</a><a class="dropdown-item" href="auth-login-cover.html"><i class="me-50" data-feather="power"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <ul class="main-search-list-defaultlist d-none">
        <li class="d-flex align-items-center"><a href="#">
                <h6 class="section-label mt-75 mb-0">Files</h6>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="me-75"><img src="/app-assets/images/icons/xls.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                    </div>
                </div><small class="search-data-size me-50 text-muted">&apos;17kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="me-75"><img src="/app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div><small class="search-data-size me-50 text-muted">&apos;11kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="me-75"><img src="/app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div><small class="search-data-size me-50 text-muted">&apos;150kb</small>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="me-75"><img src="/app-assets/images/icons/doc.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                    </div>
                </div><small class="search-data-size me-50 text-muted">&apos;256kb</small>
            </a></li>
        <li class="d-flex align-items-center"><a href="#">
                <h6 class="section-label mt-75 mb-0">Members</h6>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                <div class="d-flex align-items-center">
                    <div class="avatar me-75"><img src="/app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                <div class="d-flex align-items-center">
                    <div class="avatar me-75"><img src="/app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                <div class="d-flex align-items-center">
                    <div class="avatar me-75"><img src="/app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view-account.html">
                <div class="d-flex align-items-center">
                    <div class="avatar me-75"><img src="/app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                    </div>
                </div>
            </a></li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
        <li class="auto-suggestion justify-content-between"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                <div class="d-flex justify-content-start"><span class="me-75" data-feather="alert-circle"></span><span>No results found.</span></div>
            </a></li>
    </ul>

    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item me-auto"><a class="navbar-brand" href="/html/ltr/vertical-menu-template-dark/index.html"><span class="brand-logo">
                        <h2 class="brand-text">TODO</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item"><a class="d-flex align-items-center" href="/"><i data-feather="mail"></i><span class="menu-title text-truncate" data-i18n="Email">My Task</span></a>
                </li>
                <li class="nav-item"><a class="d-flex align-items-center" href="/"><i data-feather="star"></i><span class="menu-title text-truncate" data-i18n="Email">Important</span></a>
                </li>
                <li class="nav-item"><a class="d-flex align-items-center" href="/"><i data-feather="check"></i><span class="menu-title text-truncate" data-i18n="Email">Complete</span></a>
                </li>
                <li class="nav-item"><a class="d-flex align-items-center" href="/"><i data-feather="trash"></i><span class="menu-title text-truncate" data-i18n="Email">Deleted</span></a>
                </li>
            </ul>
        </div>
    </div>
    {{-- <div id=""> --}}
        <div class="app-content content todo-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-area-wrapper container-xxl p-0">
                <div class="sidebar-left">
                    <div class="sidebar">
                        <div class="sidebar-content todo-sidebar">
                            <div class="todo-app-menu">
                                <div class="add-task">
                                    <button type="button" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#new-task-modal">
                                        Add Task
                                    </button>
                                </div>
                                <div class="sidebar-menu-list">
                                    <div class="list-group list-group-filters">
                                        <a href="#" class="list-group-item list-group-item-action active">
                                            <i data-feather="mail" class="font-medium-3 me-50"></i>
                                            <span class="align-middle"> My Task</span>
                                        </a>
                                        {{-- <a href="#" class="list-group-item list-group-item-action">
                                            <i data-feather="star" class="font-medium-3 me-50"></i> <span class="align-middle">Important</span>
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <i data-feather="check" class="font-medium-3 me-50"></i> <span class="align-middle">Completed</span>
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <i data-feather="trash" class="font-medium-3 me-50"></i> <span class="align-middle">Deleted</span>
                                        </a> --}}
                                    </div>
                                    <div class="mt-3 px-2 d-flex justify-content-between">
                                        <h6 class="section-label mb-1">Tags</h6>
                                        <i data-feather="plus" class="cursor-pointer"></i>
                                    </div>
                                    <div class="list-group list-group-labels">
                                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                            <span class="bullet bullet-sm bullet-primary me-1"></span>Team
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                            <span class="bullet bullet-sm bullet-success me-1"></span>Low
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                            <span class="bullet bullet-sm bullet-warning me-1"></span>Medium
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                            <span class="bullet bullet-sm bullet-danger me-1"></span>High
                                        </a>
                                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                                            <span class="bullet bullet-sm bullet-info me-1"></span>Update
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content-right">
                    <div class="content-wrapper container-xxl p-0">
                        <div class="content-header row">
                        </div>
                        <div class="content-body">
                            <div class="body-content-overlay"></div>
                            <div class="todo-app-list">
                                <!-- Todo search starts -->
                                <div class="app-fixed-search d-flex align-items-center">
                                    <div class="sidebar-toggle d-block d-lg-none ms-1">
                                        <i data-feather="menu" class="font-medium-5"></i>
                                    </div>
                                    <div class="d-flex align-content-center justify-content-between w-100">
                                        <div class="input-group input-group-merge">
                                            <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
                                            <input type="text" class="form-control" id="todo-search" placeholder="Search task" aria-label="Search..." aria-describedby="todo-search" />
                                        </div>
                                    </div>
                                    <div class="dropdown">
                                        <a href="#" class="dropdown-toggle hide-arrow me-1" id="todoActions" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i data-feather="more-vertical" class="font-medium-2 text-body"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="todoActions">
                                            <a class="dropdown-item sort-asc" href="#">Sort A - Z</a>
                                            <a class="dropdown-item sort-desc" href="#">Sort Z - A</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Todo search ends -->

                                <!-- Todo List starts -->
                                <div class="todo-task-list-wrapper list-group">
                                    <ul class="todo-task-list media-list" id="todo-task-list">
                                        <li class="todo-item">
                                            <div class="todo-title-wrapper">
                                                <div class="todo-title-area">
                                                    <i data-feather="more-vertical" class="drag-icon"></i>
                                                    <div class="title-wrapper">
                                                        <div class="form-check">
                                                            <input type="checkbox" class="form-check-input" id="customCheck1" />
                                                            <label class="form-check-label" for="customCheck1"></label>
                                                        </div>
                                                        <span class="todo-title">Fix Responsiveness for new structure 💻</span>
                                                    </div>
                                                </div>
                                                <div class="todo-item-action">
                                                    <div class="badge-wrapper me-1">
                                                        <span class="badge rounded-pill badge-light-primary">Team</span>
                                                    </div>
                                                    <small class="text-nowrap text-muted me-1">Aug 08</small>
                                                    <div class="avatar">
                                                        <img src="/app-assets/images/portrait/small/avatar-s-4.jpg" alt="user-avatar" height="32" width="32" />
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="no-results">
                                        <h5>No Items Found</h5>
                                    </div>
                                </div>
                                <!-- Todo List ends -->
                            </div>

                            <!-- Right Sidebar starts -->
                            <div class="modal modal-slide-in sidebar-todo-modal fade" id="new-task-modal">
                                <div class="modal-dialog sidebar-lg">
                                    <div class="modal-content p-0">
                                        <form id="form-modal-todo" class="todo-modal needs-validation" novalidate onsubmit="return false">
                                            <div class="modal-header align-items-center mb-1">
                                                <h5 class="modal-title">Add Task</h5>
                                                <div class="todo-item-action d-flex align-items-center justify-content-between ms-auto">
                                                    <span id="change_star" class="todo-item-favorite cursor-pointer me-75" v-on:click="add.important = !add.important; check_important()">
                                                        <i data-feather="star" class="font-medium-2"></i>
                                                    </span>
                                                    <i data-feather="x" class="cursor-pointer" data-bs-dismiss="modal" stroke-width="3"></i>
                                                </div>
                                            </div>
                                            <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                                                <div class="action-tags">
                                                    <div class="mb-1">
                                                        <label for="todoTitleAdd" class="form-label">Title</label>
                                                        <input type="text" id="todoTitleAdd" name="todoTitleAdd" class="new-todo-item-title form-control" placeholder="Title" />
                                                    </div>
                                                    <div class="mb-1 position-relative">
                                                        <label for="task-assigned" class="form-label d-block">Assignee</label>
                                                        <select class="select2 form-select" id="task-assigned" name="task-assigned">
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-3.jpg" value="Phill Buffer" selected>
                                                                Phill Buffer
                                                            </option>
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-1.jpg" value="Chandler Bing">
                                                                Chandler Bing
                                                            </option>
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-4.jpg" value="Ross Geller">
                                                                Ross Geller
                                                            </option>
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-6.jpg" value="Monica Geller">
                                                                Monica Geller
                                                            </option>
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-2.jpg" value="Joey Tribbiani">
                                                                Joey Tribbiani
                                                            </option>
                                                            <option data-img="/app-assets/images/portrait/small/avatar-s-11.jpg" value="Rachel Green">
                                                                Rachel Green
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label for="task-due-date" class="form-label">Due Date</label>
                                                        <input type="text" class="form-control task-due-date" id="task-due-date" name="task-due-date" />
                                                    </div>
                                                    <div class="mb-1">
                                                        <label for="task-tag" class="form-label d-block">Tag</label>
                                                        <select class="form-select task-tag" id="task-tag" name="task-tag" multiple="multiple">
                                                            <option value="Team">Team</option>
                                                            <option value="Low">Low</option>
                                                            <option value="Medium">Medium</option>
                                                            <option value="High">High</option>
                                                            <option value="Update">Update</option>
                                                        </select>
                                                    </div>
                                                    <div class="mb-1">
                                                        <label class="form-label">Description</label>
                                                        <div id="task-desc" class="border-bottom-0" data-placeholder="Write Your Description"></div>
                                                        <div class="d-flex justify-content-end desc-toolbar border-top-0">
                                                            <span class="ql-formats me-0">
                                                                <button class="ql-bold"></button>
                                                                <button class="ql-italic"></button>
                                                                <button class="ql-underline"></button>
                                                                <button class="ql-align"></button>
                                                                <button class="ql-link"></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="my-1">
                                                    <button type="submit" class="btn btn-primary d-none add-todo-item me-1">Add</button>
                                                    <button type="button" class="btn btn-outline-secondary add-todo-item d-none" data-bs-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <button type="button" class="btn btn-primary d-none update-btn update-todo-item me-1">Update</button>
                                                    <button type="button" class="btn btn-outline-danger update-btn d-none" data-bs-dismiss="modal">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Right Sidebar ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sidenav-overlay"></div>
        <div class="drag-target"></div>
    {{-- </div> --}}
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-md-start d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2023<a class="ms-25" href="/" target="_blank">Hoàng Công Trường</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span><span class="float-md-end d-none d-md-block">Hand-crafted & Made with<i data-feather="heart"></i></span></p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="/app-assets/vendors/js/editors/quill/katex.min.js"></script>
    <script src="/app-assets/vendors/js/editors/quill/highlight.min.js"></script>
    <script src="/app-assets/vendors/js/editors/quill/quill.min.js"></script>
    <script src="/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/dragula.min.js"></script>
    <script src="/app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="/app-assets/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/app-assets/js/core/app-menu.js"></script>
    <script src="/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/app-assets/js/scripts/pages/app-todo.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
    <script>
        $(document).ready(function(){
            new Vue({
                el  : "#app",
                data : {
                    add : {
                        important : false,
                    },
                },
                created() {
                },
                methods : {
                    check_important(){
                        if(this.add.important == true) {
                            document.getElementById("change_star").classList.add('text-warning');
                        } else {
                            document.getElementById("change_star").classList.remove('text-warning');
                        }
                    }
                },
            });
        })
    </script>
</body>
<!-- END: Body-->

</html>
